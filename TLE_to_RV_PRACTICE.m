mu = 3.986004*10^14; %Earth gravitational parameter (m^3 * s^-2)
deg2rad = pi/180;

% ISS (ZARYA)             
% 1 25544U 98067A   22276.69717387  .00013953  00000+0  25471-3 0  9999
% 2 25544  51.6421 158.9831 0003016 255.2757 154.9828 15.49695164362018

i = 51.6421*deg2rad; %inclination (radians)
RAAN = 158.9831*deg2rad; %right ascension of ascending node (radians)
e = .0003016; %eccentricity
argp = 255.2757*deg2rad; %argument of periapsis/perigee (radians)


M = 154.9828*deg2rad; %mean anamoly (radians)

findE = @(E)(E - M - e*sin(E)); %function to find E with known M & e
E0 = 180*deg2rad; %initial guess for eccentric anamoly, E (degrees)
E = fzero(findE, E0);

nu = 2*atan(sqrt((1+e)/(1-e))*tan(E/2)); %true anamoly (radians)

n = 15.49695164362018; %mean motion(rev/day)
n = n*2*pi/86400; %rev/day to rad/s
a = (mu/n^2)^(1/3); %semimajor axis (meters)

[r, v] = keplerian2ijk(a, e, i/deg2rad, RAAN/deg2rad, argp/deg2rad, nu/deg2rad);

initialconditions = [r;v];

P = 2*pi/n; %orbital period (s)
% tspan = [0 2*P];
tspan = [0:P/100:2*P];
options = odeset('RelTol',1e-9);
[t,y] = ode45('TBP', tspan, initialconditions, options);

figure()
% quiver3(zeros(size(y,1),1),zeros(size(y,1),1),zeros(size(y,1),1),y(:,1),y(:,2),y(:,3))
plot3(y(:,1),y(:,2),y(:,3))
grid on
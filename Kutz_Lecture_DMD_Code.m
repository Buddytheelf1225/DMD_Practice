clear all; close all; %clc;

xi = linspace(-10,10,400);
t = linspace(0,4*pi,200);
dt = t(2) - t(1); %usually even spaced, can be generalized to any timstep technically
% [Xgrid, T] = meshgrid(xi,t); %video code
[T, Xgrid] = meshgrid(t,xi); %modified code to not be dumb


% create two spatiol temporal problems
f1 = sech(Xgrid+3).*(1*exp(i*2.3*T));
f2 = (sech(Xgrid).*tanh(Xgrid)).*(2*exp(i*2.8*T));
f = f1+f2;
% [u,s,v] = svd(f.'); %video code
[u,s,v] = svd(f);

%video code
% subplot(2,2,1), surfl(Xgrid, T, real(f1)); shading interp, colormap(gray)
% xlabel("x axis"); ylabel("time axis");
% subplot(2,2,2), surfl(Xgrid, T, real(f2)); shading interp, colormap(gray)
% xlabel("x axis"); ylabel("time axis");
% subplot(2,2,3), surfl(Xgrid, T, real(f)); shading interp, colormap(gray)
% xlabel("x axis"); ylabel("time axis");

subplot(2,2,1), surfl(T, Xgrid, real(f1)); shading interp, colormap(gray)
xlabel("time axis"); ylabel("x axis");
subplot(2,2,2), surfl(T, Xgrid, real(f2)); shading interp, colormap(gray)
xlabel("time axis"); ylabel("x axis");
subplot(2,2,3), surfl(T, Xgrid, real(f)); shading interp, colormap(gray)
xlabel("time axis"); ylabel("x axis");

figure(2)
plot(diag(s)/(sum(diag(s))),'ro')

figure(3)
subplot(2,1,1), plot(real(u(:,1:2)))
subplot(2,1,2), plot(real(v(:,1:2)))

%% DMD
dn = 8; %modification from video code
start = 1;
n_final = 200; %modified from video code

% X = f.'; %columns are time snapshots, generally best option
X = f; %columns are time snapshots, generally best option
% X1 = X(:,1:end-1); %video code
% X2 = X(:,2:end);
X1 = X(:, start : dn : n_final-dn); %modified from video code
X2 = X(:, start+dn : dn : n_final);

r = 2;
[U, S, V] = svd(X1, 'econ');
Ur = U(:,1:r);
Sr = S(1:r,1:r);
Vr = V(:,1:r);

Atilde = Ur' * X2 * Vr/Sr;
[W,D] = eig(Atilde);
Phi = X2*Vr/Sr*W; %DMD model

lambda = diag(D);
% omega = log(lambda)/dt;
omega = log(lambda)/(dn*dt); %modified from video code

figure(3)
subplot(2,1,1),hold on, plot(real(Phi), 'Linewidth',[2]);

x1 = X(:,1);
b = Phi\x1;
% t2 = linspace(0,4*pi,200);
t2 = linspace(0,4*pi,length(start:dn:n_final));
time_dynamics = zeros(r,length(t2));
for iter = 1:length(t2)
    time_dynamics(:,iter) = (b.* exp(omega*t2(iter)));
end

X_dmd = Phi*time_dynamics;
figure(1)
%video code
% subplot(2,2,4), surfl(Xgrid, T, real(X_dmd).'); shading interp, colormap(gray)
% xlabel("x axis"); ylabel("time axis");
% subplot(2,2,4), surfl(T, Xgrid, real(X_dmd)); shading interp, colormap(gray)
% xlabel("time axis"); ylabel("x axis");
subplot(2,2,4), surfl(T(:,start:dn:n_final), Xgrid(:,start:dn:n_final), real(X_dmd)); shading interp, colormap(gray)
xlabel("time axis"); ylabel("x axis");
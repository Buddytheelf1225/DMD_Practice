clear all; close all; %clc;

addpath("C:\Users\jcale\OneDrive - The Ohio State University\AAA Current Year\AE 5626 Orbital Mechanics\functions")

%% Define dataset

mu = 3.986004*10^14; %Earth gravitational parameter (m^3 * s^-2)
deg2rad = pi/180;
revday2radsec = 2*pi/24/3600;

% ISS (ZARYA)             
% 1 25544U 98067A   22276.69717387  .00013953  00000+0  25471-3 0  9999
% 2 25544  51.6421 158.9831 0003016 255.2757 154.9828 15.49695164362018

% i = 51.6421*deg2rad; %inclination (radians)
% RAAN = 158.9831*deg2rad; %right ascension of ascending node (radians)
% e = .0003016; %eccentricity
% argp = 255.2757*deg2rad; %argument of periapsis/perigee (radians)
% 
% M = 154.9828*deg2rad; %mean anamoly (radians)
% 
% n = 15.49695164362018*revday2radsec; %mean motion(rad/s)


% TEST EXAMPLE FROM HWK 3 G2
% ans in m & m/s:
r_ans = [-.31056; 1.0441; -.28125]*10^7;
v_ans = [-5.3241; -.77472; 2.9495]*10^3;


i = 30*deg2rad;
RAAN = 260*deg2rad;
e = .25;
argp = 120*deg2rad;

f = 90*deg2rad;
E = 2*atan(sqrt((1-e)/(1+e))*tan(f/2));
M = E - e*sin(E);

a = 1.2*10^7;
n = sqrt(mu/a^3);

% Calculate position and velocity vectors in inertial frame
[r v] = TLE2rv(i, RAAN, e, argp, M, n);

% Propogate forward with ODE45 and plot
initCond = [r;v];

P = 2*pi/n;
deltaT = 1*60; %in seconds
tspan = [1*60 : deltaT : 24*3600];

options = odeset('AbsTol',1e-9,'RelTol',1e-6);
[t_full,y_full] = ode45('TBP', tspan, initCond, options);

t_full = t_full';
y_full = y_full';

% figure()
% plot3(y_full(1,:),y_full(2,:),y_full(3,:))
% grid on

%% SVD
i_8_hr = find(t_full == 8*3600-deltaT);
y = y_full(:,1:i_8_hr);
t = t_full(1:i_8_hr);

yi = [1:6]';
[T,Ygrid] = meshgrid(t,yi);

% create two spatiol temporal problems
% f1 = sech(Xgrid+3).*(1*exp(i*2.3*T));
% f2 = (sech(Xgrid).*tanh(Xgrid)).*(2*exp(i*2.8*T));
% f = f1+f2;
% [u,s,v] = svd(f.'); %video code
% [u,s,v] = svd(f);
[u,s,v] = svd(y);

%video code
% subplot(2,2,1), surfl(Xgrid, T, real(f1)); shading interp, colormap(gray)
% xlabel("x axis"); ylabel("time axis");
% subplot(2,2,2), surfl(Xgrid, T, real(f2)); shading interp, colormap(gray)
% xlabel("x axis"); ylabel("time axis");
% subplot(2,2,3), surfl(Xgrid, T, real(f)); shading interp, colormap(gray)
% xlabel("x axis"); ylabel("time axis");

% subplot(2,2,1), surfl(T, Xgrid, real(f1)); shading interp, colormap(gray)
% xlabel("time axis"); ylabel("x axis");
% subplot(2,2,2), surfl(T, Xgrid, real(f2)); shading interp, colormap(gray)
% xlabel("time axis"); ylabel("x axis");
subplot(2,1,1), surfl(T(1:3,:), Ygrid(1:3,:), y(1:3,:)); shading interp, colormap(gray)
xlabel("time axis"); ylabel("x axis"); title("Position");
subplot(2,1,2), surfl(T(4:6,:), Ygrid(4:6,:), y(4:6,:)); shading interp, colormap(gray)
xlabel("time axis"); ylabel("x axis"); title("Velocity");

figure(2)
plot(diag(s)/(sum(diag(s))),'ro')

figure(3)
subplot(2,1,1), plot(real(u(:,1:3)))
subplot(2,1,2), plot(real(v(:,1:3)))

%% DMD
dn = 1; %modification from video code
start = 1;
n_final = length(y); %modified from video code


X = y; %columns are time snapshots, generally best option
X1 = X(:, start : dn : n_final-dn); %modified from video code
X2 = X(:, start+dn : dn : n_final);

r = 2;
[U, S, V] = svd(X1, 'econ');
Ur = U(:,1:r);
Sr = S(1:r,1:r);
Vr = V(:,1:r);

Atilde = Ur' * X2 * Vr/Sr;
[W,D] = eig(Atilde);
Phi = X2*Vr/Sr*W; %DMD model

lambda = diag(D);
% omega = log(lambda)/dt;
omega = log(lambda)/(dn*dt); %modified from video code

figure(3)
subplot(2,1,1),hold on, plot(real(Phi), 'Linewidth',[2]);

x1 = X(:,1);
b = Phi\x1;
% t2 = linspace(0,4*pi,200);
t2 = linspace(0,4*pi,length(start:dn:n_final));
time_dynamics = zeros(r,length(t2));
for iter = 1:length(t2)
    time_dynamics(:,iter) = (b.* exp(omega*t2(iter)));
end

X_dmd = Phi*time_dynamics;
figure(1)
%video code
% subplot(2,2,4), surfl(Xgrid, T, real(X_dmd).'); shading interp, colormap(gray)
% xlabel("x axis"); ylabel("time axis");
% subplot(2,2,4), surfl(T, Xgrid, real(X_dmd)); shading interp, colormap(gray)
% xlabel("time axis"); ylabel("x axis");
subplot(2,2,4), surfl(T(:,start:dn:n_final), Ygrid(:,start:dn:n_final), real(X_dmd)); shading interp, colormap(gray)
xlabel("time axis"); ylabel("x axis");